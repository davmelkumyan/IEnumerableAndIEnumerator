﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsCSharp
{
    class Element
    {
        public string Name { get; set; }
        public int Age { get; set; }

        public Element(string name, int age)
        {
            this.Name = name;
            this.Age = age;
        }
    }
}
