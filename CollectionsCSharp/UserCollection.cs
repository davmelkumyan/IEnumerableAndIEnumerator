﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsCSharp
{
    class UserCollection : IEnumerable, IEnumerator
    {
        Element[] element;
        public int Count { get; }

        public UserCollection(int count)
        {
            Count = count;
            element = new Element[count];
            element[0] = new Element("David", 21);
            element[1] = new Element("Andrew", 18);
            element[2] = new Element("Georgey", 15);
            element[3] = new Element("Gramon", 11);
        }
        int position = -1;

        public object Current { get => element[position]; }

        bool IEnumerator.MoveNext()
        {
            if(position < element.Length - 1)
            {
                position++;
                return true;
            }
            else
            {
                Reset();
                return false;
            }
        }

        public void Reset()
        {
            position = -1;
        }




        IEnumerator IEnumerable.GetEnumerator()
        {
            return this as IEnumerator;
        }
    }
}
